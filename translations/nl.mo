��          �   %   �      0     1  $   I     n     �     �     �  -   �     �  
                        0     7  
   D  r   O     �  C   �          $     -  w   5     �  �  �     P  $   k     �     �     �     �  /   �       
   #     .     ;     C     X     _  	   m  m   w     �  N   �     F  
   W  
   b  �   m     �                                        	                                                                         
              Allowed payment methods Buckaroo (iDeal, creditcard, PayPal) Buckaroo API Information Buckaroo Gateway URL Buckaroo Merchant key Buckaroo Secret Key Buckaroo payments (iDeal, creditcard, PayPal) Cash ticket Creditcard Development Giropay Pay by Buckaroo. PayPal Paysafe card Production Select the transaction mode to use for communication with buckaroo. This specifies if it is a test payment or not. Submit order This information is required for Ubercart to interact with buckaroo Transaction mode Transfer Warrant Your order could not be completed because Buckaroo returned an exception. Please try again or contact the service desk. iDeal Project-Id-Version: Ubercart Buckaroo payments
POT-Creation-Date: 2012-07-12 13:59+0200
PO-Revision-Date: 2012-07-12 14:03+0100
Last-Translator: Fabian de Rijk <fabian@finalist.nl>
Language-Team: Finalist <fabian@finalist.nl>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Poedit-Language: Dutch
X-Poedit-Country: NETHERLANDS
 Toegestane betaal methodes Buckaroo (iDeal, creditcard, PayPal) Buckaroo API Informatie Buckaroo Gateway URL Buckaroo Merchant key Buckaroo Secret Key Buckaroo betalingen (iDeal, creditcard, PayPal) Cash ticket Creditcard Ontwikkeling Giropay Betaal via Buckaroo. PayPal Paysafe kaart Productie Selecteer de transactie modus voor communicatie met Buckaroo. Dit bepaalt of de betaling een test is of niet. Plaats bestelling Deze informatie is nodig voor Ubercart om te kunnen communiceren met Buckaroo. Transactie modus Overmaking Machtiging Uw bestelling kon niet bevestigd worden door een onbekend probleem bij Buckaroo. Probeer opnieuw, of neem contact op met de servicedesk. iDeal 